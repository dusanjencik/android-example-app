# Testovací aplikace #

Jedná se o testovací aplikaci, která stáhne feed [The New York Times](http://api.nytimes.com/svc/search/v2/articlesearch.json?q=new+york+times&sort=newest&api-key=sample-key) a uloží jej do databáze. Následně pomocí content provideru z ní čte. Na úvodní obrazovce je seznam článků. Při načtení obrazovky se seznam automaticky aktualizuje (netvoří se duplikáty). Aktualizaci lze provést i swipe gestem dolů. Každá položka seznamu obsahuje *titulek, relativní čas a obrázek*. Po kliknutí na jednu z položek se zobrazí nová obrazovka s detailem článku. Je zde zobrazen *titulek, obrázek, datum, perex, zdroj a seznam klíčových slov*. Odkaz na článek lze sdílet.

## Využité knihovny ##
* Support library
* Glide
* ButterKnife
* Retrofit
* Multiline collapsing toolbar

![Screenshot_20160628-124535.png](https://bitbucket.org/repo/nMLMdX/images/515445115-Screenshot_20160628-124535.png)


![Screenshot_20160628-124549.png](https://bitbucket.org/repo/nMLMdX/images/3556716838-Screenshot_20160628-124549.png)