package cz.dusanjencik.exampleapp;

import android.app.Application;
import android.content.Context;

/**
 * @author Dušan Jenčík dusanjencik@gmail.com
 *         created 26.06.16
 */
public class App extends Application {
	public static final String TAG = "App";

	private static App sInstance = null;

	public static Context getAppContext() {
		if (sInstance == null) {
			throw new RuntimeException("Context is not known!");
		}
		return sInstance.getBaseContext();
	}

	@Override
	public void onCreate() {
		super.onCreate();
		sInstance = this;
		onAppLoaded();
	}

	private void onAppLoaded() {
		// Here can be added some checks on load.
	}
}
