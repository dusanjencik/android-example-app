package cz.dusanjencik.exampleapp;

/**
 * @author Dušan Jenčík dusanjencik@gmail.com
 *         created 26.06.16
 */
public class Constants {

	public static final String CONTENT_PROVIDER_AUTHORITY = "cz.dusanjencik.exampleapp";
	public static final String NO_IMAGE_URL               = "https://upload.wikimedia.org/wikipedia/commons/thumb/6/6c/No_image_3x4.svg/2000px-No_image_3x4.svg.png";
	public static final String BASE_URL_NY_TIMES_API      = "http://api.nytimes.com";
	public static final String BASE_URL_NY_TIMES          = "http://nytimes.com";
}
