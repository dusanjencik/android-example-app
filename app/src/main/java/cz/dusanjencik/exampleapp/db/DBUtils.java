package cz.dusanjencik.exampleapp.db;

import android.content.ContentValues;
import android.content.Context;
import android.support.annotation.NonNull;

import java.util.ArrayList;

import cz.dusanjencik.exampleapp.db.contracts.NewsContract;
import cz.dusanjencik.exampleapp.models.News;
import cz.dusanjencik.exampleapp.models.NewsResponse;
import cz.dusanjencik.exampleapp.utils.PrefUtils;

/**
 * Helper class for easier manipulating with DB.
 *
 * @author Dušan Jenčík dusanjencik@gmail.com
 *         created 27.06.16
 */
public class DBUtils {
	private DBUtils() {
		// not instantiable
	}

	/**
	 * Inserting a data from {@link NewsResponse} to DB.
	 */
	public static void insertToDB(Context context, @NonNull NewsResponse newsResponse) {
		int                      newsIndex             = 0;
		ContentValues[]          newsContentValues     = new ContentValues[newsResponse.getNewsList().size()];
		ArrayList<ContentValues> keywordsContentValues = new ArrayList<>();

		for (News news : newsResponse.getNewsList()) {
			String        remote_id = news.get_id();
			ContentValues cv        = news.buildContentValues();
			newsContentValues[newsIndex++] = cv;
			for (News.Keyword keyword : news.getKeywords()) {
				keywordsContentValues.add(keyword.buildContentValues(remote_id));
			}
		}
		context.getContentResolver().bulkInsert(NewsContract.News.CONTENT_URI, newsContentValues);
		context.getContentResolver().bulkInsert(NewsContract.Keywords.CONTENT_URI,
				keywordsContentValues.toArray(new ContentValues[keywordsContentValues.size()]));
		PrefUtils.setSetDataDownloaded(true);
	}

	/**
	 * Deleting of content of all tables.
	 */
	public static void deleteContentOfAllTables(Context context) {
		PrefUtils.setSetDataDownloaded(false);
		context.getContentResolver().delete(NewsContract.News.CONTENT_URI, null, null);
		context.getContentResolver().delete(NewsContract.Keywords.CONTENT_URI, null, null);
	}
}
