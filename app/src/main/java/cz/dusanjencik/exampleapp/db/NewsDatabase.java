package cz.dusanjencik.exampleapp.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;

import static cz.dusanjencik.exampleapp.db.contracts.NewsContract.KeywordsColumns;
import static cz.dusanjencik.exampleapp.db.contracts.NewsContract.News;
import static cz.dusanjencik.exampleapp.db.contracts.NewsContract.NewsColumns;
import static cz.dusanjencik.exampleapp.db.contracts.NewsContract.PATH_KEYWORDS;
import static cz.dusanjencik.exampleapp.db.contracts.NewsContract.PATH_NEWS;

/**
 * Class of DB table definitions.
 *
 * @author Dušan Jenčík dusanjencik@gmail.com
 *         created 26.06.16
 */
public class NewsDatabase extends SQLiteOpenHelper {
	public static final String TAG = "NewsDatabase";

	private static final String DATABASE_NAME = "news.db";

	private static final int VER_LAUNCH       = 100; // 1.0
	private static final int DATABASE_VERSION = VER_LAUNCH;

	public interface Tables {
		String NEWS     = PATH_NEWS;
		String KEYWORDS = PATH_KEYWORDS;
	}

	private interface References {
		String NEWS_ID = "REFERENCES " + Tables.NEWS + "(" + News.NEWS_REMOTE_ID + ")";
	}

	public NewsDatabase(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL("CREATE TABLE " + Tables.NEWS + " ("
				+ BaseColumns._ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
				+ NewsColumns.NEWS_REMOTE_ID + " TEXT NOT NULL,"
				+ NewsColumns.NEWS_WEB_URL + " TEXT NOT NULL,"
				+ NewsColumns.NEWS_LEAD_PARAGRAPH + " TEXT,"
				+ NewsColumns.NEWS_SOURCE + " TEXT,"
				+ NewsColumns.NEWS_PUB_DATE + " INTEGER NOT NULL,"
				+ NewsColumns.NEWS_THUMBNAIL_URL + " TEXT,"
				+ NewsColumns.NEWS_BIG_IMAGE_URL + " TEXT,"
				+ NewsColumns.NEWS_MAIN_HEADLINE + " TEXT,"
				+ NewsColumns.NEWS_PRINT_HEADLINE + " TEXT,"
				+ "UNIQUE (" + NewsColumns.NEWS_REMOTE_ID + ") ON CONFLICT REPLACE)"
		);

		db.execSQL("CREATE TABLE " + Tables.KEYWORDS + " ("
				+ BaseColumns._ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
				+ KeywordsColumns.KEYWORDS_FK_NEWS_ID + " TEXT NOT NULL " + References.NEWS_ID + ","
				+ KeywordsColumns.KEYWORDS_RANK + " TEXT,"
				+ KeywordsColumns.KEYWORDS_IS_MAJOR + " TEXT,"
				+ KeywordsColumns.KEYWORDS_NAME + " TEXT,"
				+ KeywordsColumns.KEYWORDS_VALUE + " TEXT,"
				+ "UNIQUE (" + KeywordsColumns.KEYWORDS_FK_NEWS_ID + ","
				+ KeywordsColumns.KEYWORDS_VALUE + ") ON CONFLICT REPLACE)"
		);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL("DROP TABLE IF EXISTS " + Tables.NEWS);
		db.execSQL("DROP TABLE IF EXISTS " + Tables.KEYWORDS);
		onCreate(db);
	}

	public static void deleteDatabase(Context context) {
		context.deleteDatabase(DATABASE_NAME);
	}
}
