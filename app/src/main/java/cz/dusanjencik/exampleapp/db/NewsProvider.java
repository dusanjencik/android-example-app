package cz.dusanjencik.exampleapp.db;

import android.content.ContentProvider;
import android.content.ContentProviderOperation;
import android.content.ContentProviderResult;
import android.content.ContentValues;
import android.content.Context;
import android.content.OperationApplicationException;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.ArrayList;

import cz.dusanjencik.exampleapp.db.NewsDatabase.Tables;
import cz.dusanjencik.exampleapp.db.contracts.NewsContract;
import cz.dusanjencik.exampleapp.db.helpers.SelectionBuilder;

/**
 * Custom content provider to accessing to DB.
 *
 * @author Dušan Jenčík dusanjencik@gmail.com
 *         created 26.06.16
 */
public class NewsProvider extends ContentProvider {
	public static final String TAG = "NewsProvider";

	private NewsDatabase mOpenHelper;

	private static final UriMatcher sUriMatcher = buildUriMather();

	private static final int NEWS          = 100;
	private static final int NEWS_ID       = 101;
	private static final int NEWS_KEYWORDS = 102;

	private static final int KEYWORDS    = 200;
	private static final int KEYWORDS_ID = 201;

	/**
	 * Basic content resolver endpoints.
	 */
	private static UriMatcher buildUriMather() {
		final UriMatcher matcher   = new UriMatcher(UriMatcher.NO_MATCH);
		final String     authority = NewsContract.CONTENT_AUTHORITY;

		matcher.addURI(authority, "news", NEWS);
		matcher.addURI(authority, "news/*", NEWS_ID); // remote id
		matcher.addURI(authority, "news/*/keywords", NEWS_KEYWORDS); // remote id

		matcher.addURI(authority, "keywords", KEYWORDS);
		matcher.addURI(authority, "keywords/*", KEYWORDS_ID);

		return matcher;
	}


	@Override
	public boolean onCreate() {
		mOpenHelper = new NewsDatabase(getContext());
		return mOpenHelper.getWritableDatabase() != null;
	}

	private void deleteDatabase() {
		mOpenHelper.close();
		NewsDatabase.deleteDatabase(getContext());
		mOpenHelper = new NewsDatabase(getContext());
	}

	@Nullable
	@Override
	public String getType(@NonNull Uri uri) {
		final int match = sUriMatcher.match(uri);
		switch (match) {
			case NEWS:
				return NewsContract.News.CONTENT_TYPE;
			case NEWS_ID:
				return NewsContract.News.CONTENT_ITEM_TYPE;
			case NEWS_KEYWORDS:
				return NewsContract.Keywords.CONTENT_TYPE;
			case KEYWORDS:
				return NewsContract.Keywords.CONTENT_TYPE;
			case KEYWORDS_ID:
				return NewsContract.Keywords.CONTENT_ITEM_TYPE;
			default:
				throw new UnsupportedOperationException("Unknows uri: " + uri);
		}
	}

	@Nullable
	@Override
	public Cursor query(@NonNull Uri uri, String[] projections, String selection, String[] selectionArgs, String sortOrder) {
		final SQLiteDatabase   db      = mOpenHelper.getReadableDatabase();
		final SelectionBuilder builder = buildSelection(uri);
		return builder.where(selection, selectionArgs).query(db, projections, sortOrder);
	}

	@Nullable
	@Override
	public Uri insert(@NonNull Uri uri, ContentValues contentValues) {
		final SQLiteDatabase db    = mOpenHelper.getWritableDatabase();
		final int            match = sUriMatcher.match(uri);
		switch (match) {
			case NEWS: {
				db.insertOrThrow(Tables.NEWS, null, contentValues);
				notifyChange(uri);
				return NewsContract.News.buildNewsUri(contentValues.getAsString(NewsContract.News.NEWS_REMOTE_ID));
			}
			case KEYWORDS: {
				db.insertOrThrow(Tables.KEYWORDS, null, contentValues);
				notifyChange(uri);
				return NewsContract.Keywords.buildKeywordsUri(contentValues.getAsString(NewsContract.Keywords._ID));
			}
			default:
				throw new UnsupportedOperationException("Unknown uri: " + uri);
		}
	}

	@Override
	public int delete(@NonNull Uri uri, String selection, String[] selectionArgs) {
		if (uri == NewsContract.BASE_CONTENT_URI) {
			deleteDatabase();
			notifyChange(uri);
			return 1;
		}
		final SQLiteDatabase   db        = mOpenHelper.getWritableDatabase();
		final SelectionBuilder builder   = buildSelection(uri);
		int                    returnVal = builder.where(selection, selectionArgs).delete(db);
		notifyChange(uri);
		return returnVal;
	}

	@Override
	public int update(@NonNull Uri uri, ContentValues contentValues, String selection, String[] selectionArgs) {
		final SQLiteDatabase   db        = mOpenHelper.getWritableDatabase();
		final SelectionBuilder builder   = buildSelection(uri);
		int                    returnVal = builder.where(selection, selectionArgs).update(db, contentValues);
		notifyChange(uri);
		return returnVal;
	}

	/**
	 * Automatically called in batch operations on content resolver.
	 */
	@NonNull
	@Override
	public ContentProviderResult[] applyBatch(@NonNull ArrayList<ContentProviderOperation> operations)
			throws OperationApplicationException {
		final SQLiteDatabase db = mOpenHelper.getWritableDatabase();
		db.beginTransaction();
		try {
			final int                     numOperations = operations.size();
			final ContentProviderResult[] results       = new ContentProviderResult[numOperations];
			for (int i = 0; i < numOperations; ++i) {
				results[i] = operations.get(i).apply(this, results, i);
			}
			db.setTransactionSuccessful();
			return results;
		} finally {
			db.endTransaction();
		}
	}

	private void notifyChange(Uri uri) {
		Context context = getContext();
		if (context == null) return;
		context.getContentResolver().notifyChange(uri, null, false);
	}

	/**
	 * Build a simple {@link SelectionBuilder} to match the requested
	 * {@link Uri}. This is usually enough to support {@link #insert},
	 * {@link #update}, and {@link #delete} operations.
	 */
	private SelectionBuilder buildSelection(Uri uri) {
		final SelectionBuilder builder = new SelectionBuilder();
		final int              match   = sUriMatcher.match(uri);
		switch (match) {
			case NEWS:
				return builder.table(Tables.NEWS);
			case NEWS_ID: {
				final String newsId = NewsContract.News.getNewsId(uri);
				return builder.table(Tables.NEWS).where(NewsContract.News.NEWS_REMOTE_ID + "=?", newsId);
			}
			case NEWS_KEYWORDS: {
				final String newsId = NewsContract.News.getNewsId(uri);
				return builder.table(Tables.KEYWORDS).where(NewsContract.Keywords.KEYWORDS_FK_NEWS_ID + "=?", newsId);
			}
			case KEYWORDS:
				return builder.table(Tables.KEYWORDS);
			case KEYWORDS_ID: {
				final String keywordsId = NewsContract.Keywords.getKeywordsId(uri);
				return builder.table(Tables.KEYWORDS).where(NewsContract.Keywords._ID + "=?", keywordsId);
			}
			default:
				throw new UnsupportedOperationException("Unknows uri: " + uri);
		}
	}
}
