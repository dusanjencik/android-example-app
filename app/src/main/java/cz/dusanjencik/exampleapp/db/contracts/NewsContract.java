package cz.dusanjencik.exampleapp.db.contracts;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.BaseColumns;
import android.support.annotation.NonNull;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;

import cz.dusanjencik.exampleapp.Constants;

/**
 * A contract with definition tables, their columns and basic Loaders.
 *
 * @author Dušan Jenčík dusanjencik@gmail.com
 *         created 26.06.16
 */
public class NewsContract {

	/**
	 * For table news.
	 */
	public interface NewsColumns {
		String NEWS_REMOTE_ID      = "news_remote_id"; // id from remote server
		String NEWS_WEB_URL        = "news_web_url";
		String NEWS_LEAD_PARAGRAPH = "news_lead_paragraph";
		String NEWS_SOURCE         = "news_source";
		String NEWS_PUB_DATE       = "news_pub_date";

		// multimedia
		String NEWS_THUMBNAIL_URL = "news_thumbnail_url";
		String NEWS_BIG_IMAGE_URL = "news_big_image_url";

		// Headline
		String NEWS_MAIN_HEADLINE  = "news_main_headline";
		String NEWS_PRINT_HEADLINE = "news_print_headline";
	}

	/**
	 * For table keywords
	 */
	public interface KeywordsColumns {
		String KEYWORDS_FK_NEWS_ID = "keywords_fk_news_id"; // foreign key to news table local id
		String KEYWORDS_RANK       = "keywords_rank";
		String KEYWORDS_IS_MAJOR   = "keywords_is_major";
		String KEYWORDS_NAME       = "keywords_name";
		String KEYWORDS_VALUE      = "keywords_value";
	}

	public static final String CONTENT_AUTHORITY = Constants.CONTENT_PROVIDER_AUTHORITY;
	public static final Uri    BASE_CONTENT_URI  = Uri.parse("content://" + CONTENT_AUTHORITY);

	public static final String PATH_NEWS     = "news";
	public static final String PATH_KEYWORDS = "keywords";

	/**
	 * Class for accessing to columns of news table and definition of basic Loaders.
	 */
	public static class News implements NewsColumns, BaseColumns {
		public static final Uri    CONTENT_URI       = BASE_CONTENT_URI.buildUpon().appendPath(PATH_NEWS).build();
		public static final String CONTENT_TYPE      = "vnd.android.cursor.dir/vnd.exampleapp.news";
		public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/vnd.exampleapp.news";

		public static Uri buildNewsUri(String newsId) {
			return CONTENT_URI.buildUpon().appendPath(newsId).build();
		}

		public static String getNewsId(Uri uri) {
			return uri.getPathSegments().get(1);
		}

		/**
		 * Loader for listing all of news.
		 *
		 * @return List of news.
		 */
		public static Loader<Cursor> getListLoader(Context context) {
			return new CursorLoader(
					context,   // Parent activity context
					CONTENT_URI,        // Table to query
					new String[]{
							_ID,
							NEWS_REMOTE_ID,
							NEWS_MAIN_HEADLINE,
							NEWS_PRINT_HEADLINE,
							NEWS_THUMBNAIL_URL,
							NEWS_PUB_DATE},     // Projection to return
					null,            // No selection clause
					null,            // No selection arguments
					NEWS_PUB_DATE + " DESC"             // Default sort order
			);
		}

		/**
		 * Loader for fetching detailed data about news.
		 *
		 * @param newsId news remote id.
		 * @return One news.
		 */
		public static Loader<Cursor> getDetailLoader(Context context, @NonNull String newsId) {
			return new CursorLoader(
					context,   // Parent activity context
					CONTENT_URI.buildUpon().appendPath(newsId).build(),        // Table to query
					new String[]{
							_ID,
							NEWS_REMOTE_ID,
							NEWS_MAIN_HEADLINE,
							NEWS_PRINT_HEADLINE,
							NEWS_BIG_IMAGE_URL,
							NEWS_LEAD_PARAGRAPH,
							NEWS_SOURCE,
							NEWS_WEB_URL,
							NEWS_PUB_DATE},     // Projection to return
					null,            // No selection clause
					null,            // No selection arguments
					null            // No sort order
			);
		}

		/**
		 * Loader for fetching a list of keywords correspond to the news.
		 *
		 * @param newsId news remote id.
		 * @return List of keywords.
		 */
		public static Loader<Cursor> getDetailKeywordsLoader(Context context, @NonNull String newsId) {
			return new CursorLoader(
					context,   // Parent activity context
					CONTENT_URI.buildUpon().appendPath(newsId).appendPath("keywords").build(),        // Table to query
					new String[]{
							Keywords._ID,
							Keywords.KEYWORDS_FK_NEWS_ID,
							Keywords.KEYWORDS_RANK,
							Keywords.KEYWORDS_IS_MAJOR,
							Keywords.KEYWORDS_NAME,
							Keywords.KEYWORDS_VALUE},     // Projection to return
					null,            // No selection clause
					null,            // No selection arguments
					Keywords.KEYWORDS_VALUE + " ASC"            // Sort by keyword name
			);
		}
	}

	/**
	 * Class for accessing to columns of keywords table.
	 */
	public static class Keywords implements KeywordsColumns, BaseColumns {
		public static final Uri    CONTENT_URI       = BASE_CONTENT_URI.buildUpon().appendPath(PATH_KEYWORDS).build();
		public static final String CONTENT_TYPE      = "vnd.android.cursor.dir/vnd.exampleapp.keyword";
		public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/vnd.exampleapp.keyword";

		public static Uri buildKeywordsUri(String keywordsId) {
			return CONTENT_URI.buildUpon().appendPath(keywordsId).build();
		}

		public static String getKeywordsId(Uri uri) {
			return uri.getPathSegments().get(1);
		}


	}
}
