package cz.dusanjencik.exampleapp.models;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.support.annotation.NonNull;
import android.text.format.DateUtils;

import java.util.Date;
import java.util.List;

import cz.dusanjencik.exampleapp.Constants;
import cz.dusanjencik.exampleapp.db.contracts.NewsContract;
import cz.dusanjencik.exampleapp.db.contracts.NewsContract.KeywordsColumns;
import cz.dusanjencik.exampleapp.db.contracts.NewsContract.NewsColumns;
import cz.dusanjencik.exampleapp.net.NYTimesService;

/**
 * Model class for News entity.
 *
 * @author Dušan Jenčík dusanjencik@gmail.com
 *         created 25.06.16
 */
public class News {

	String _id, web_url, lead_paragraph, source;
	List<Multimedia> multimedia;
	Headline         headline;
	List<Keyword>    keywords;
	Date             pub_date;

	private String bigImageUrl, thumbnailUrl;

	public ContentValues buildContentValues() {
		ContentValues cv = new ContentValues();
		cv.put(NewsColumns.NEWS_REMOTE_ID, _id);
		cv.put(NewsColumns.NEWS_WEB_URL, web_url);
		cv.put(NewsColumns.NEWS_LEAD_PARAGRAPH, lead_paragraph);
		cv.put(NewsColumns.NEWS_SOURCE, source);
		cv.put(NewsColumns.NEWS_PUB_DATE, pub_date.getTime());
		cv.put(NewsColumns.NEWS_THUMBNAIL_URL, getImageUrl(true));
		cv.put(NewsColumns.NEWS_BIG_IMAGE_URL, getImageUrl(false));
		cv.put(NewsColumns.NEWS_MAIN_HEADLINE, headline.main);
		cv.put(NewsColumns.NEWS_PRINT_HEADLINE, headline.print_headline);
		return cv;
	}

	@Override
	public String toString() {
		return "News{" +
				"_id='" + _id + '\'' +
				", web_url='" + web_url + '\'' +
				", lead_paragraph='" + lead_paragraph + '\'' +
				", source='" + source + '\'' +
				", multimedia=" + multimedia +
				", headline=" + headline +
				", keywords=" + keywords +
				", pub_date=" + pub_date +
				", bigImageUrl='" + bigImageUrl + '\'' +
				", thumbnailUrl='" + thumbnailUrl + '\'' +
				'}';
	}

	/**
	 * Save locally the url of thumbnail and big image.
	 */
	private void prepareImages() {
		if (multimedia != null) {
			for (Multimedia m : multimedia) {
				if ("xlarge".equals(m.subtype)) {
					bigImageUrl = m.getUrl();
					continue;
				}
				if ("thumbnail".equals(m.subtype))
					thumbnailUrl = m.getUrl();
			}
		}
		if (bigImageUrl == null) bigImageUrl = Constants.NO_IMAGE_URL;
		if (thumbnailUrl == null) thumbnailUrl = Constants.NO_IMAGE_URL;
	}

	public String getImageUrl(boolean thumbnail) {
		if (bigImageUrl == null || thumbnailUrl == null) prepareImages();
		return thumbnail ? thumbnailUrl : bigImageUrl;
	}

	public String getHeadline() {
		if (headline.print_headline != null) return headline.print_headline;
		return headline.main;
	}

	public String get_id() {
		return _id;
	}

	public String getWeb_url() {
		return web_url;
	}

	public String getLead_paragraph() {
		return lead_paragraph;
	}

	public String getSource() {
		return source;
	}

	/**
	 * Transform the publication date to readable relative format.
	 */
	public String getRelativePubDateHumanReadable() {
		long now = System.currentTimeMillis();
		return String.valueOf(DateUtils.getRelativeTimeSpanString(pub_date.getTime(), now, DateUtils.MINUTE_IN_MILLIS));
	}

	public String getPubDate(Context context) {
		return DateUtils.formatDateTime(context, pub_date.getTime(), DateUtils.FORMAT_SHOW_DATE | DateUtils.FORMAT_NUMERIC_DATE | DateUtils.FORMAT_SHOW_YEAR);
	}

	public List<Keyword> getKeywords() {
		return keywords;
	}

	public static News parseCursor(Cursor c) {
		News news = new News();
		news._id = parse(c, NewsContract.News.NEWS_REMOTE_ID);
		news.headline = new Headline();
		news.headline.main = parse(c, NewsContract.News.NEWS_MAIN_HEADLINE);
		news.headline.print_headline = parse(c, NewsContract.News.NEWS_PRINT_HEADLINE);
		if (c.getColumnIndex(NewsContract.News.NEWS_PUB_DATE) != -1)
			news.pub_date = new Date(c.getLong(c.getColumnIndex(NewsContract.News.NEWS_PUB_DATE)));
		news.web_url = parse(c, NewsContract.News.NEWS_WEB_URL);
		news.lead_paragraph = parse(c, NewsContract.News.NEWS_LEAD_PARAGRAPH);
		news.source = parse(c, NewsContract.News.NEWS_SOURCE);

		news.thumbnailUrl = parse(c, NewsContract.News.NEWS_THUMBNAIL_URL);
		news.bigImageUrl = parse(c, NewsContract.News.NEWS_BIG_IMAGE_URL);

		return news;
	}

	private static String parse(Cursor c, String columnName) {
		int index = c.getColumnIndex(columnName);
		if (index != -1) return c.getString(index);
		return null;
	}

	/**
	 * Model class for Multimedia param.
	 */
	public static class Multimedia {
		int width, height;
		String url, subtype;

		public String getUrl() {
			return url == null ? Constants.NO_IMAGE_URL :
					NYTimesService.BASE_URL_IMAGE + "/" + url;
		}

		@Override
		public String toString() {
			return "Multimedia{" +
					"width=" + width +
					", height=" + height +
					", url='" + url + '\'' +
					", subtype='" + subtype + '\'' +
					'}';
		}
	}

	/**
	 * Model class for Headline param.
	 */
	public static class Headline {
		String main, print_headline;

		@Override
		public String toString() {
			return "Headline{" +
					"main='" + main + '\'' +
					", print_headline='" + print_headline + '\'' +
					'}';
		}
	}

	/**
	 * Model class for Keyword entity.
	 */
	public static class Keyword {
		String newsId, rank, is_major, name, value;

		public ContentValues buildContentValues(@NonNull String remote_id) {
			ContentValues cv = new ContentValues();
			cv.put(KeywordsColumns.KEYWORDS_FK_NEWS_ID, remote_id);
			cv.put(KeywordsColumns.KEYWORDS_RANK, rank);
			cv.put(KeywordsColumns.KEYWORDS_IS_MAJOR, is_major);
			cv.put(KeywordsColumns.KEYWORDS_NAME, name);
			cv.put(KeywordsColumns.KEYWORDS_VALUE, value);
			return cv;
		}

		public String getName() {
			return name;
		}

		public String getValue() {
			return value;
		}

		public static Keyword parseCursor(Cursor c) {
			Keyword keyword = new Keyword();
			keyword.newsId = parse(c, KeywordsColumns.KEYWORDS_FK_NEWS_ID);
			keyword.rank = parse(c, KeywordsColumns.KEYWORDS_RANK);
			keyword.is_major = parse(c, KeywordsColumns.KEYWORDS_IS_MAJOR);
			keyword.name = parse(c, KeywordsColumns.KEYWORDS_NAME);
			keyword.value = parse(c, KeywordsColumns.KEYWORDS_VALUE);

			return keyword;
		}

		@Override
		public String toString() {
			return "Keyword{" +
					"rank='" + rank + '\'' +
					", is_major='" + is_major + '\'' +
					", name='" + name + '\'' +
					", value='" + value + '\'' +
					'}';
		}
	}
}
