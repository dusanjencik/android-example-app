package cz.dusanjencik.exampleapp.models;

import java.util.List;

/**
 * Model class for NewsResponse entity.
 *
 * @author Dušan Jenčík dusanjencik@gmail.com
 *         created 25.06.16
 */
public class NewsResponse {
	Response response;
	String   copyright;

	public List<News> getNewsList() {
		return response.docs;
	}

	@Override
	public String toString() {
		return "NewsResponse{" +
				"response=" + response +
				", copyright='" + copyright + '\'' +
				'}';
	}

	public static class Response {
		List<News> docs;

		@Override
		public String toString() {
			return "Response{" +
					"docs=" + docs +
					'}';
		}
	}
}
