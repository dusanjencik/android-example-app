package cz.dusanjencik.exampleapp.net;

import cz.dusanjencik.exampleapp.Constants;
import cz.dusanjencik.exampleapp.models.NewsResponse;
import retrofit2.Call;
import retrofit2.http.GET;

/**
 * A definition class for requesting.
 *
 * @author Dušan Jenčík dusanjencik@gmail.com
 *         created 25.06.16
 */
public interface NYTimesService {

	String BASE_URL       = Constants.BASE_URL_NY_TIMES_API;
	String BASE_URL_IMAGE = Constants.BASE_URL_NY_TIMES;

	@GET("/svc/search/v2/articlesearch.json?q=new+york+times&sort=newest&api-key=sample-key")
	Call<NewsResponse> searchArticles();
}
