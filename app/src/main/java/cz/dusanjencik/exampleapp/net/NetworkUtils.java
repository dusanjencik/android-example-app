package cz.dusanjencik.exampleapp.net;

import android.content.Context;
import android.net.ConnectivityManager;

/**
 * Helper class for network operations.
 *
 * @author Dušan Jenčík dusanjencik@gmail.com
 *         created 27.06.16
 */
public class NetworkUtils {
	public static final String TAG = "NetworkUtils";

	private NetworkUtils() {
		// not instantiable
	}

	public static boolean isNetworkConnected(final Context context) {
		ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		return cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnectedOrConnecting();
	}
}
