package cz.dusanjencik.exampleapp.net;

import com.google.gson.GsonBuilder;

import cz.dusanjencik.exampleapp.models.NewsResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


/**
 * Helper class for requesting.
 *
 * @author Dušan Jenčík dusanjencik@gmail.com
 *         created 25.06.16
 */
public class Requester {
	public static final String TAG = "Requester";

	private Requester() {
		// not instantiable
	}

	/**
	 * Call the {@link NYTimesService} endpoint to search last articles.
	 */
	public static void searchArticles(final ResponseListener<NewsResponse> listener) {
		Retrofit retrofit = new Retrofit.Builder()
				.baseUrl(NYTimesService.BASE_URL)
				.addConverterFactory(GsonConverterFactory.create(
						new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").create()))
				.build();
		NYTimesService nyTimesService = retrofit.create(NYTimesService.class);

		Call<NewsResponse> jsonObjectCall = nyTimesService.searchArticles();
		jsonObjectCall.enqueue(new Callback<NewsResponse>() {
			@Override
			public void onResponse(Call<NewsResponse> call, Response<NewsResponse> response) {
				listener.onSuccess(response.body());
			}

			@Override
			public void onFailure(Call<NewsResponse> call, Throwable t) {
				listener.onFailure();
			}
		});
	}

	public interface ResponseListener<T> {
		void onSuccess(T data);

		void onFailure();
	}
}
