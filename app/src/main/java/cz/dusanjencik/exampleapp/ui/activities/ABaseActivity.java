package cz.dusanjencik.exampleapp.ui.activities;

import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import butterknife.BindView;
import butterknife.ButterKnife;
import cz.dusanjencik.exampleapp.R;

/**
 * Base class with definition of basic activity methods.
 *
 * @author Dušan Jenčík dusanjencik@gmail.com
 *         created 25.06.16
 */
public abstract class ABaseActivity extends AppCompatActivity {
	public static final String TAG = "ABaseActivity";

	@BindView(R.id.toolbar) Toolbar mToolbar;

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(getLayoutId());
		ButterKnife.bind(this);
		setSupportActionBar(mToolbar);
		setTitle(getTitle());
	}

	protected void showBackInToolbar(){
		ActionBar actionBar = getSupportActionBar();
		if (actionBar != null) {
			actionBar.setDisplayHomeAsUpEnabled(true);
		}
	}

	@Override
	public void setTitle(CharSequence title) {
		mToolbar.setTitle(title);
	}

	@LayoutRes
	protected abstract int getLayoutId();
}
