package cz.dusanjencik.exampleapp.ui.activities;

import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import butterknife.BindView;
import butterknife.OnClick;
import cz.dusanjencik.exampleapp.R;
import cz.dusanjencik.exampleapp.db.DBUtils;
import cz.dusanjencik.exampleapp.db.contracts.NewsContract.News;
import cz.dusanjencik.exampleapp.models.NewsResponse;
import cz.dusanjencik.exampleapp.net.NetworkUtils;
import cz.dusanjencik.exampleapp.net.Requester;
import cz.dusanjencik.exampleapp.ui.adapters.NewsAdapter;
import cz.dusanjencik.exampleapp.ui.dialogs.DialogFactory;
import cz.dusanjencik.exampleapp.ui.helpers.SimpleDividerItemDecorator;
import cz.dusanjencik.exampleapp.utils.PrefUtils;

/**
 * Main screen. There is a list of news.
 *
 * @author Dušan Jenčík dusanjencik@gmail.com
 *         created 25.06.16
 */
public class MainActivity extends ABaseActivity implements LoaderManager.LoaderCallbacks<Cursor> {
	public static final String TAG = "MainActivity";

	private static final int NEWS_LOADER_DB = 1;

	@BindView(R.id.news_list)      RecyclerView       mRecyclerView;
	@BindView(R.id.swipeContainer) SwipeRefreshLayout mSwipeRefreshLayout;
	@BindView(R.id.no_data)        View               mNoDataView;

	private NewsAdapter mAdapter;

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		mAdapter = new NewsAdapter(this, null);
		mRecyclerView.setAdapter(mAdapter);
		mRecyclerView.addItemDecoration(new SimpleDividerItemDecorator(this));
		mSwipeRefreshLayout.setColorSchemeResources(
				android.R.color.holo_red_light,
				android.R.color.holo_orange_light,
				android.R.color.holo_green_light,
				android.R.color.holo_blue_bright
		);
		mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
			@Override
			public void onRefresh() {
				if (NetworkUtils.isNetworkConnected(MainActivity.this))
					downloadNews();
				else {
					mSwipeRefreshLayout.setRefreshing(false);
					showNoInternetConnection();
				}
			}
		});

		if (PrefUtils.isDataDownloaded()) {
			getSupportLoaderManager().initLoader(NEWS_LOADER_DB, null, this);
			if (NetworkUtils.isNetworkConnected(this)) {
				downloadNews();
			}
		} else {
			if (NetworkUtils.isNetworkConnected(this)) {
				downloadNews();
			} else
				showNoInternetConnection();
		}
	}

	private void showNoInternetConnection() {
		Snackbar.make(mRecyclerView, R.string.no_internet_connection, Snackbar.LENGTH_LONG)
				.show();
	}

	@Override
	protected int getLayoutId() {
		return R.layout.activity_main;
	}

	private void refreshing(final boolean refreshing) {
		if (mSwipeRefreshLayout != null) {
			mSwipeRefreshLayout.post(new Runnable() {
				@Override
				public void run() {
					mSwipeRefreshLayout.setRefreshing(refreshing);
				}
			});
		}
	}

	@OnClick(R.id.download_again)
	void downloadNews() {
		refreshing(true);
		Requester.searchArticles(new Requester.ResponseListener<NewsResponse>() {
			@Override
			public void onSuccess(final NewsResponse data) {
				new AsyncTask<Void, Void, Void>() {
					@Override
					protected Void doInBackground(Void... voids) {
						DBUtils.insertToDB(MainActivity.this, data);
						return null;
					}

					@Override
					protected void onPostExecute(Void aVoid) {
						super.onPostExecute(aVoid);
						getSupportLoaderManager().initLoader(NEWS_LOADER_DB, null, MainActivity.this);
					}
				}.execute();
			}

			@Override
			public void onFailure() {
				refreshing(false);
				if (NetworkUtils.isNetworkConnected(MainActivity.this)) {
					DialogFactory.showErrorConnectionDialog(MainActivity.this, new DialogFactory.DialogListener() {
						@Override
						public void onButtonClick() {
							downloadNews();
						}

						@Override
						public void onCancel() {
							Snackbar.make(mRecyclerView, R.string.download_failed, Snackbar.LENGTH_LONG)
									.show();
						}
					});
				}else{
					showNoInternetConnection();
				}
			}
		});
	}

	@Override
	public Loader<Cursor> onCreateLoader(int id, Bundle args) {
		switch (id) {
			case NEWS_LOADER_DB: {
				refreshing(true);
				return News.getListLoader(this);
			}
			default:
				// An invalid id was passed in
				return null;
		}
	}

	@Override
	public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
		if (mAdapter.getItemCount() == 0 && data.getCount() == 0) {
			mNoDataView.setVisibility(View.VISIBLE);
		} else mNoDataView.setVisibility(View.GONE);
		mAdapter.changeCursor(data);
		refreshing(false);
	}

	@Override
	public void onLoaderReset(Loader<Cursor> loader) {
	}
}
