package cz.dusanjencik.exampleapp.ui.activities;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import net.opacapp.multilinecollapsingtoolbar.CollapsingToolbarLayout;

import butterknife.BindView;
import butterknife.OnClick;
import cz.dusanjencik.exampleapp.R;
import cz.dusanjencik.exampleapp.db.contracts.NewsContract;
import cz.dusanjencik.exampleapp.models.News;
import cz.dusanjencik.exampleapp.ui.adapters.KeywordsAdapter;

/**
 * Detail screen. There are more information about news. Keywords are listed too.
 *
 * @author Dušan Jenčík dusanjencik@gmail.com
 *         created 25.06.16
 */
public class NewsDetailActivity extends ABaseActivity implements LoaderManager.LoaderCallbacks<Cursor> {

	public static final String ARG_NEWS_ID = "arg_news_id";

	private static final int NEWS_LOADER_DB_DETAIL          = 1;
	private static final int NEWS_LOADER_DB_DETAIL_KEYWORDS = 2;

	@BindView(R.id.toolbar_layout)      CollapsingToolbarLayout mCollapsingToolbarLayout;
	@BindView(R.id.item_lead_paragraph) TextView                mLeadParagraphView;
	@BindView(R.id.app_bar)             View                    mAppBar;
	@BindView(R.id.detail_image)        ImageView               mImageView;
	@BindView(R.id.pub_time)            TextView                mPubTime;
	@BindView(R.id.source)              TextView                mSourceView;
	@BindView(R.id.keywords_list)       RecyclerView            mKeywordsList;
	@BindView(R.id.headline_text)       TextView                mLeadParagraphHeadline;
	@BindView(R.id.keywords_text)       TextView                mKeywordsHeadline;

	private String          mNewsRemoteId;
	private News            mNews;
	private KeywordsAdapter mKeywordsAdapter;

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		showBackInToolbar();
		mKeywordsAdapter = new KeywordsAdapter(this, null);
		mKeywordsList.setAdapter(mKeywordsAdapter);
		LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
		mKeywordsList.setLayoutManager(layoutManager);

		if ((mNewsRemoteId = getIntent().getStringExtra(ARG_NEWS_ID)) != null) {
			getSupportLoaderManager().initLoader(NEWS_LOADER_DB_DETAIL, null, this);
			getSupportLoaderManager().initLoader(NEWS_LOADER_DB_DETAIL_KEYWORDS, null, this);
		}

	}

	@Override
	public void setTitle(CharSequence title) {
		if (mCollapsingToolbarLayout != null)
			mCollapsingToolbarLayout.setTitle(title);
	}

	@Override
	protected int getLayoutId() {
		return R.layout.activity_item_detail;
	}

	@OnClick(R.id.fab)
	public void onFabClick(View view) {
		if (mNews == null) return;
		Intent sendIntent = new Intent();
		sendIntent.setAction(Intent.ACTION_SEND);
		sendIntent.putExtra(Intent.EXTRA_TEXT, mNews.getWeb_url());
		sendIntent.setType("text/plain");
		startActivity(sendIntent);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == android.R.id.home) {
			navigateUpTo(new Intent(this, MainActivity.class));
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public Loader<Cursor> onCreateLoader(int id, Bundle args) {
		switch (id) {
			case NEWS_LOADER_DB_DETAIL:
				return NewsContract.News.getDetailLoader(this, mNewsRemoteId);
			case NEWS_LOADER_DB_DETAIL_KEYWORDS:
				return NewsContract.News.getDetailKeywordsLoader(this, mNewsRemoteId);
			default:
				// An invalid id was passed in
				return null;
		}
	}

	@Override
	public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
		switch (loader.getId()) {
			case NEWS_LOADER_DB_DETAIL: {
				if (data.moveToFirst()) {
					mNews = News.parseCursor(data);
					setTitle(mNews.getHeadline());
					if(mNews.getLead_paragraph() == null){
						mLeadParagraphView.setVisibility(View.GONE);
						mLeadParagraphHeadline.setVisibility(View.GONE);
					}
					mLeadParagraphView.setText(mNews.getLead_paragraph());
					mSourceView.setText(mNews.getSource());
					mPubTime.setText(mNews.getPubDate(this));
					Glide.with(this)
							.load(mNews.getImageUrl(false))
							.into(mImageView);
				} else
					Snackbar.make(mCollapsingToolbarLayout, R.string.error_loading_db, Snackbar.LENGTH_LONG).show();
				break;
			}
			case NEWS_LOADER_DB_DETAIL_KEYWORDS: {
				if (data.getCount() == 0) mKeywordsHeadline.setVisibility(View.GONE);
				else mKeywordsHeadline.setVisibility(View.VISIBLE);
				mKeywordsAdapter.changeCursor(data);
				break;
			}
		}
	}

	@Override
	public void onLoaderReset(Loader<Cursor> loader) {

	}
}
