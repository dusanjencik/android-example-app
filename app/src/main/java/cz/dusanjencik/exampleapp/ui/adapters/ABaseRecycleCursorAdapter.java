package cz.dusanjencik.exampleapp.ui.adapters;

import android.content.Context;
import android.database.Cursor;
import android.support.annotation.LayoutRes;
import android.support.v4.widget.CursorAdapter;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import cz.dusanjencik.exampleapp.ui.adapters.view_holders.ABaseViewHolder;

/**
 * Basic class for reading a cursor by {@link RecyclerView}.
 *
 * @author Dušan Jenčík dusanjencik@gmail.com
 *         created 28.06.16
 */
public abstract class ABaseRecycleCursorAdapter<VH extends ABaseViewHolder<T>, T> extends RecyclerView.Adapter<VH> {
	public static final String TAG = "ABaseRecycleCursorAdapter";

	/**
	 * Because RecyclerView.Adapter doesn't implement cursors, this is a wrap of CursorAdapter
	 * which can do it.
	 */
	private CursorAdapter mCursorAdapter;
	private Context       mContext;

	public ABaseRecycleCursorAdapter(Context context, Cursor c) {
		mContext = context;
		mCursorAdapter = new CursorAdapter(mContext, c, true) {
			@Override
			public View newView(Context context, Cursor cursor, ViewGroup parent) {
				return LayoutInflater.from(parent.getContext())
						.inflate(getLayoutId(), parent, false);
			}

			@Override
			public void bindView(View view, Context context, Cursor cursor) {
				ABaseRecycleCursorAdapter.this.bindView(view, cursor);
			}
		};
	}

	@LayoutRes
	protected abstract int getLayoutId();

	protected abstract void bindView(View view, Cursor cursor);

	@Override
	public VH onCreateViewHolder(ViewGroup parent, int viewType) {
		View v = mCursorAdapter.newView(mContext, mCursorAdapter.getCursor(), parent);
		return onCreateNewViewHolder(v);
	}

	protected abstract VH onCreateNewViewHolder(View view);

	@Override
	public void onBindViewHolder(VH holder, int position) {
		mCursorAdapter.getCursor().moveToPosition(position);
		mCursorAdapter.bindView(holder.mView, mContext, mCursorAdapter.getCursor());
	}

	@Override
	public int getItemCount() {
		return mCursorAdapter.getCount();
	}

	public void changeCursor(Cursor c) {
		mCursorAdapter.changeCursor(c);
		notifyDataSetChanged();
	}
}
