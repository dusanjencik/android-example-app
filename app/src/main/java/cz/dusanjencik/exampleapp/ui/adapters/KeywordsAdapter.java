package cz.dusanjencik.exampleapp.ui.adapters;

import android.content.Context;
import android.database.Cursor;
import android.view.View;

import cz.dusanjencik.exampleapp.R;
import cz.dusanjencik.exampleapp.models.News;
import cz.dusanjencik.exampleapp.ui.adapters.view_holders.KeywordsViewHolder;

/**
 * @author Dušan Jenčík dusanjencik@gmail.com
 *         created 28.06.16
 */
public class KeywordsAdapter extends ABaseRecycleCursorAdapter<KeywordsViewHolder, News.Keyword> {
	public static final String TAG = "KeywordsAdapter";

	public KeywordsAdapter(Context context, Cursor c) {
		super(context, c);
	}

	@Override
	protected int getLayoutId() {
		return R.layout.keywords_item_list_content;
	}

	@Override
	protected void bindView(View view, Cursor cursor) {
		News.Keyword       keyword    = News.Keyword.parseCursor(cursor);
		KeywordsViewHolder viewHolder = new KeywordsViewHolder(view);
		viewHolder.onBind(keyword);
	}

	@Override
	protected KeywordsViewHolder onCreateNewViewHolder(View view) {
		return new KeywordsViewHolder(view);
	}
}
