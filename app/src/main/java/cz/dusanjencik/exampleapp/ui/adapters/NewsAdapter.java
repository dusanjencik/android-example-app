package cz.dusanjencik.exampleapp.ui.adapters;

import android.content.Context;
import android.database.Cursor;
import android.view.View;

import cz.dusanjencik.exampleapp.R;
import cz.dusanjencik.exampleapp.models.News;
import cz.dusanjencik.exampleapp.ui.adapters.view_holders.NewsViewHolder;
import cz.dusanjencik.exampleapp.utils.ActivityUtils;

/**
 * @author Dušan Jenčík dusanjencik@gmail.com
 *         created 28.06.16
 */
public class NewsAdapter extends ABaseRecycleCursorAdapter<NewsViewHolder, News> {
	public static final String TAG = "NewsAdapter";

	public NewsAdapter(Context context, Cursor c) {
		super(context, c);
	}

	@Override
	protected int getLayoutId() {
		return R.layout.news_item_list_content;
	}

	@Override
	protected void bindView(View view, Cursor cursor) {
		final News           news       = News.parseCursor(cursor);
		final NewsViewHolder viewHolder = new NewsViewHolder(view);
		viewHolder.onBind(news);
		viewHolder.mView.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				ActivityUtils.startNewsActivity(view.getContext(), news.get_id());
			}
		});
	}

	@Override
	protected NewsViewHolder onCreateNewViewHolder(View view) {
		return new NewsViewHolder(view);
	}
}
