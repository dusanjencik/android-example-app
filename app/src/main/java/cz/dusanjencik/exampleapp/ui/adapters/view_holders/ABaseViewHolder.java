package cz.dusanjencik.exampleapp.ui.adapters.view_holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import butterknife.ButterKnife;

/**
 * Base class for ViewHolders.
 *
 * @author Dušan Jenčík dusanjencik@gmail.com
 *         created 25.06.16
 */
public abstract class ABaseViewHolder<T> extends RecyclerView.ViewHolder {
	public static final String TAG = "ABaseViewHolder";

	public final View mView;

	public ABaseViewHolder(View itemView) {
		super(itemView);
		mView = itemView;
		ButterKnife.bind(this, mView);
	}

	public abstract void onBind(T item);
}
