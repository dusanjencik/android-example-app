package cz.dusanjencik.exampleapp.ui.adapters.view_holders;

import android.view.View;
import android.widget.TextView;

import butterknife.BindView;
import cz.dusanjencik.exampleapp.R;
import cz.dusanjencik.exampleapp.models.News;

/**
 * @author Dušan Jenčík dusanjencik@gmail.com
 *         created 28.06.16
 */
public class KeywordsViewHolder extends ABaseViewHolder<News.Keyword> {
	public static final String TAG = "KeywordsViewHolder";

	@BindView(R.id.keywordTitle) TextView mTitle;

	public KeywordsViewHolder(View itemView) {
		super(itemView);
	}

	@Override
	public void onBind(News.Keyword item) {
		mTitle.setText(item.getValue());
	}
}
