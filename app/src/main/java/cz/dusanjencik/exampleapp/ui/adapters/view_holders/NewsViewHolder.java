package cz.dusanjencik.exampleapp.ui.adapters.view_holders;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import butterknife.BindView;
import cz.dusanjencik.exampleapp.R;
import cz.dusanjencik.exampleapp.models.News;

/**
 * @author Dušan Jenčík dusanjencik@gmail.com
 *         created 26.06.16
 */
public class NewsViewHolder extends ABaseViewHolder<News> {
	public static final String TAG = "NewsViewHolder";

	@BindView(R.id.item_thumbnail) ImageView mThumbnailView;
	@BindView(R.id.title)          TextView  mTitleView;
	@BindView(R.id.pub_time)       TextView  mPubTimeView;

	public NewsViewHolder(View itemView) {
		super(itemView);
	}

	@Override
	public void onBind(News item) {
		mTitleView.setText(item.getHeadline());
		mPubTimeView.setText(item.getRelativePubDateHumanReadable());
		Glide.with(mView.getContext())
				.load(item.getImageUrl(true))
				.into(mThumbnailView);
	}
}
