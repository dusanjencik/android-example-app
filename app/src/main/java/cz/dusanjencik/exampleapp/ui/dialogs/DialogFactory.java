package cz.dusanjencik.exampleapp.ui.dialogs;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;

import cz.dusanjencik.exampleapp.R;

/**
 * Helper class for creating dialogs.
 *
 * @author Dušan Jenčík dusanjencik@gmail.com
 *         created 26.06.16
 */
public class DialogFactory {
	private DialogFactory() {
		// not instantiable
	}

	public static void showErrorConnectionDialog(Context context, final DialogListener listener) {
		new AlertDialog.Builder(context)
				.setTitle(R.string.error)
				.setMessage(R.string.download_failed)
				.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialogInterface, int i) {
						listener.onCancel();
					}
				})
				.setNegativeButton(R.string.try_again, new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialogInterface, int i) {
						listener.onButtonClick();
					}
				})
				.setCancelable(true)
				.setOnCancelListener(new DialogInterface.OnCancelListener() {
					@Override
					public void onCancel(DialogInterface dialogInterface) {
						listener.onCancel();
					}
				})
				.setIcon(android.R.drawable.ic_dialog_alert)
				.show();
	}

	public interface DialogListener {
		void onButtonClick();

		void onCancel();
	}
}
