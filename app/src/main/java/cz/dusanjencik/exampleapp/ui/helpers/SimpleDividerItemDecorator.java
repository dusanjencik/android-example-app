package cz.dusanjencik.exampleapp.ui.helpers;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import cz.dusanjencik.exampleapp.R;

/**
 * Class for creating separators/dividers in {@link RecyclerView}.
 *
 * @author Dušan Jenčík dusanjencik@gmail.com
 *         created 28.06.16
 */
public class SimpleDividerItemDecorator extends RecyclerView.ItemDecoration {
	public static final String TAG = "SimpleDividerItemDecorator";
	private Drawable mDivider;

	public SimpleDividerItemDecorator(Context context) {
		mDivider = ContextCompat.getDrawable(context, R.drawable.line_divider);
	}

	@Override
	public void onDrawOver(Canvas c, RecyclerView parent, RecyclerView.State state) {
		int left  = parent.getPaddingLeft();
		int right = parent.getWidth() - parent.getPaddingRight();

		int childCount = parent.getChildCount();
		for (int i = 0; i < childCount; i++) {
			View child = parent.getChildAt(i);

			RecyclerView.LayoutParams params = (RecyclerView.LayoutParams) child.getLayoutParams();

			int top    = child.getBottom() + params.bottomMargin;
			int bottom = top + mDivider.getIntrinsicHeight();

			mDivider.setBounds(left, top, right, bottom);
			mDivider.draw(c);
		}
	}
}