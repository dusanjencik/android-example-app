package cz.dusanjencik.exampleapp.utils;

import android.content.Context;
import android.content.Intent;

import cz.dusanjencik.exampleapp.ui.activities.NewsDetailActivity;


/**
 * Helper class for Activity operations.
 *
 * @author Dušan Jenčík dusanjencik@gmail.com
 *         created 26.06.16
 */
public class ActivityUtils {
	private ActivityUtils() {
		// not instantiable
	}

	public static void startNewsActivity(Context context, String _id) {
		Intent intent = new Intent(context, NewsDetailActivity.class);
		intent.putExtra(NewsDetailActivity.ARG_NEWS_ID, _id);
		context.startActivity(intent);
	}
}
