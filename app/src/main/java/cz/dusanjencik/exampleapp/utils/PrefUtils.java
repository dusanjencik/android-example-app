package cz.dusanjencik.exampleapp.utils;

import android.annotation.SuppressLint;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import java.lang.ref.WeakReference;

import cz.dusanjencik.exampleapp.App;

/**
 * Helper class for saving/loading from {@link SharedPreferences}.
 *
 * @author Dušan Jenčík dusanjencik@gmail.com
 *         created 26.06.16
 */
public class PrefUtils {
	public static final String TAG = "PrefUtils";
	private static WeakReference<SharedPreferences> sPrefs;

	public static final String PREF_DATA_DOWNLOADED = "pref_data_downloaded";

	private PrefUtils() {
		// not instantiable
	}

	private static SharedPreferences getPrefs() {
		if (sPrefs == null || sPrefs.get() == null) {
			sPrefs = new WeakReference<>(PreferenceManager.getDefaultSharedPreferences(App.getAppContext()));
		}
		return sPrefs.get();
	}

	@SuppressLint("CommitPrefEdits")
	private static SharedPreferences.Editor getEditorAndParseValue(String key, Object value) {
		SharedPreferences.Editor editor = getPrefs().edit();
		if (value instanceof String) editor.putString(key, (String) value);
		else if (value instanceof Integer) editor.putInt(key, (Integer) value);
		else if (value instanceof Boolean) editor.putBoolean(key, (Boolean) value);
		else if (value instanceof Long) editor.putLong(key, (Long) value);
		else
			DebugLog.logE(TAG, "Cannot save " + (value == null ? "null" : (value.getClass().getSimpleName())));
		return editor;
	}

	private static SharedPreferences.Editor getEditorAndParseValue(SharedPreferences.Editor editor, String key, Object value) {
		if (value instanceof String) editor.putString(key, (String) value);
		else if (value instanceof Integer) editor.putInt(key, (Integer) value);
		else if (value instanceof Boolean) editor.putBoolean(key, (Boolean) value);
		else if (value instanceof Long) editor.putLong(key, (Long) value);
		else
			DebugLog.logE(TAG, "Cannot save " + (value == null ? "null" : (value.getClass().getSimpleName())));
		return editor;
	}

	/**
	 * Used for batch saving.
	 */
	private static SharedPreferences.Editor save(SharedPreferences.Editor editor, String key, Object value) {
		return getEditorAndParseValue(editor, key, value);
	}

	private static void saveImmediatelySync(String key, Object value) {
		getEditorAndParseValue(key, value).apply();
	}

	private static void saveImmediatelyAsync(String key, Object value) {
		getEditorAndParseValue(key, value).commit();
	}

	public static void setSetDataDownloaded(boolean downloaded) {
		saveImmediatelySync(PREF_DATA_DOWNLOADED, downloaded);
	}

	public static boolean isDataDownloaded() {
		return getPrefs().getBoolean(PREF_DATA_DOWNLOADED, false);
	}
}
